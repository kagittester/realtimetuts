$(document).ready(function () {
    var socket = io();
    
    socket.on('updates', function(track) {
        $('#' + track.id + ' .likes').text(track.likes);
        $('#' + track.id + ' .unlikes').text(track.unlikes);
        $('#' + track.id + ' .title').text(track.title);
        $('#' + track.id + ' .trkPosition').text(track.position);
        $('#' + track.id + ' .duration').text(track.duration);
        
    });
    socket.on('tracks', function(track) {

	    $(".tracks").append("<li class='track' id='" + track.id + "'>" +
                "<span class='position'>" + ($( "li.track" ).length-1) + "</span>" +
                "<div class='vote'>" +
                    "<div class='btnVote'>" +
                        "<span class='btnLike'><i class='fa fa-thumbs-up fa-2x'></i></span>" +
                        "<span class='numVotes likes'>" + track.likes + "</span>" +
                    "</div>" +
                    "<div class='btnVote'>" +
                        "<span class='btnUnlike'><i class='fa fa-thumbs-down fa-2x'></i></span>" +
                        "<span class='numVotes unlikes'>" + track.unlikes + "</span>" +
                    "</div>" +
                "</div>" +
                "<span class='title'>" + track.title + "</span>" +
                "<span class='duration'>" + track.position + "</span>" +
                "<span class='duration'>" + track.duration + "</span></li>");
	});

    $('.tracks').on('click', 'span.btnLike', function (e) {
        var trackId = $(this).parent('div').parent('div').parent('li')[0].id;
        $.ajax({
            type: 'PUT',
            url: '/track/like/' + trackId
        });
    });
    $('.tracks').on('click', 'span.btnUnlike', function (e) {
        var trackId = $(this).parent('div').parent('div').parent('li')[0].id;
        $.ajax({
            type: 'PUT',
            url: '/track/unlike/' + trackId
        });
    });
    $('#form').on('submit', function (event) {
        event.preventDefault();
        var input = $('#title');
        var t = input.val();
        if(!t || t.trim().length === 0) {
            alert('The title is required');
            return false;
        } else {
            $.ajax({
                type: 'POST',
                url: '/track',
                data: {
                    title: t
                },
                success: function(data) {
                    input.val('');
                }
            });
        }
    });
});