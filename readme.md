install rethinkdb [download installer from website]

run rethinkdb server
windows : cd installpath | .\rethinkdb.exe
mac : rethinkdb
- access on localhost:8080

node app.js  
- access on localhost:3000

rethinkdb changefeed testing
- goto localhost:8080

click on dataexplorer and input query

//get all data 
- r.db('musicplayer').table('tracks')

// insert data
- r.db('musicplayer').table('tracks').insert({title: "mplayer", position: 30, duration: 132})


//update title for specific id
- r.db('musicplayer').table('tracks').get("enter an id from previos results here").update({title: "testing"})

check localhost:3000 to see instant update

A realtime app tutorials using the opensource realtime db : rethinkdb with socketio and nodejs
## EXERCISE
Setup a playlist array of track names & durations and select a track from the list with timer to increment the current position. 
Send details to server every 5secs to update db with timestamp
if current position is greater or equal to duration, goto next track and set current position to zero
The playlist has uniqueID, so current position should overwrite previous current position

This should automatically update on frontend as db is updated