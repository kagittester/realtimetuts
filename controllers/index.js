var model = require('../models/tracks');

var vote = function (req, res, action) {
    var track = {
        id:req.params.id
    };
    model.updateTrack(track, action, function (success, result) {
        if (success) res.json({
            status: 'OK'
        });
        else res.json({
            status: 'Error'
        });
    });
}

module.exports = function (app) {
    app.get('/', function (req, res) {
        model.getTracks(function (result) {
            res.render('index', {
                tracks: result
            });
        });
    });

    app.post('/track', function (req, res) {
        var track = {
            title:req.body.title,
            duration:1,
            position:0,
            likes:0,
            unlikes:0
        };
        model.saveTrack(track, function (success, result) {
            if (success) res.json({
                status: 'OK'
            });
            else res.json({
                status: 'Error'
            });
        });
    });

    app.put('/track/like/:id', function (req, res) {
       vote(req, res, 'likes');
    });

    app.put('/track/unlike/:id', function (req, res) {
        vote(req, res, 'unlikes');
    });

    app.put('/track/title/:id', function (req, res) {
        vote(req, res, 'title');
    });
    app.put('/track/duration/:id', function (req, res) {
        vote(req, res, 'duration');
    });

    app.put('/track/position/:id', function (req, res) {
        vote(req, res, 'position');
    });

};